# Test Documentation Site 

Welcome to the my test documentation site. This is a static website built using Hugo. 

To learn more about building static websites using Hugo head to their [official website](https://gohugo.io/). To learn more about deploying a Hugo website on GitLab pages follow [this link](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/).


To build your first Hugo website follow the step by step tutorial from Giraffe Academy below:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/qtIqKaDlqXo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


## Contents
 - [Product 1](product1/index.md)
 - [Product 2](product2/index.md)
