# Product 2

Welcome to the documentation of Product 2, the latest product in the making at Lorem Ipsum & Co.

#### Announcement

![Beta Launch Banner](images/beta-launch.jpeg "We're in Beta, hurray!")

We are currently in the **Beta** phase of our product launch. If you want gain _early_ access to our product, write an email to our support team at support@loremipsum.com to learn more about becoming a Beta user.


## Contents
- [Vision](vision.md)
- [Team](team/index.md)
