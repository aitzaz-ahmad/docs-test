# The Team

## Head of Product
![Grandmaster](images/oogway.png "Grandmaster")

## Architect
![Master](images/shifu.png "Shifu")

## Project Manager
![Lord Shen](images/lord-shen.png "Shifu")

## DevOps Lead
![Wolf](images/wolf.jpg "Image Wolf") 

## QA Lead 
![Tai Lung](images/tai-lung.png "Tai Lung")

## Dev Lead
![Tigress](images/tigress.jpg "Tigress")

## Developers
![Mantis](images/mantis.jpg "Mantis") *C++ Developer*
![Monkey](images/monkey.png "Monkey") *Java Developer*
![Crane](images/crane.png "Crane") *Front-end Developer*
![Po](images/po.jpg "Po") *Back-end Developer*
